import React, {} from 'react';
import {Modal} from "react-bootstrap";
export default function ViewCard(props) {

    return (
        <div>
            <Modal 
                show={props.show}
                onHide ={props.onHide}
                animation = {false}
                dialogClassName="modal-90w"
                aria-labelledby="example-custom-modal-styling-title"
            >
                <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">
                    <h1>{props.ViewName}</h1>
                </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ul>
                        Gender: {props.ViewGender} <br/>
                        Job: <li>{props.ViewJob}</li>
                    </ul>
                </Modal.Body>
                <Modal.Footer>
                    <button className="rounded mb-0 border-danger" CLAS onClick={props.onHide}>Close</button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

