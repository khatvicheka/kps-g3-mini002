import React, { Component } from "react";
import { Button, Form, Col, Table,Card, ListGroup, DropdownButton, Dropdown} from "react-bootstrap";
import Pagination from "react-js-pagination"; //P
import "bootstrap/dist/css/bootstrap.min.css";
import { default as UUID } from "uuid";
import Moment from 'react-moment';
import 'moment/locale/km';
import ViewIfo from "./ViewIfo";
import ViewCard from './ViewCard';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      newRow: 0, //for adding new row
      index: "",
      person: [],
      activePage: 1, // P
      viewID:'',
      viewName:'',
      viewAge:'',
      viewGender:'',
      viewJob:'',
      show:null,
      action:false,
      isClicked: true,

      //Validation
      fields: {},
      errors: {},

      //update
      btnUpdate:false,
      IdUpdate:''

    };
  
  }
  //clear
 

    //Validate 
    handleValidation(){
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
  
      //Name
      if(!fields["name"]){
        formIsValid = false;
        errors["name"] = "Cannot be empty";
      }
  
      if(typeof fields["name"] !== "undefined"){
        if(!fields["name"].match(/^[a-zA-Z\s]+$/)){
          formIsValid = false;
          errors["name"] = "Only letters";
        }
      }
  
      //Password
      if(!fields["password"]){
        formIsValid = false;
        errors["password"] = "Cannot be empty";
      }
  
      if(typeof fields["password"] !== "undefined"){
        if(!fields["password"].match(/^[0-9]*$/)){
          formIsValid = false;
          errors["password"] = "*Number Only"
        }
      }
  
      this.setState({errors: errors});
      return formIsValid;
    }
  
    handleChange(field, e){
      let fields = this.state.fields;
      fields[field] = e.target.value;
      this.setState({fields});
    }

  componentWillMount() {
    this.setState({
      id: UUID.v4(),
    });
  }
  componentDidMount() {
    this.name.focus();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { gender } = this.form;
    const { job } = this.form;
    const checkboxArray = Array.prototype.slice.call(job);
    const checkedCheckboxes = checkboxArray.filter((input) => input.checked);
    const checkedCheckboxesValues = checkedCheckboxes.map(
      (input) => input.value);

    let person = this.state.person;
    let id = this.state.id.substring(0, 5);
    let name = this.name.value;
    let age = this.age.value;
    let genderCheck = gender.value;
    let jobChoose = checkedCheckboxesValues.map((v, i) => <li key={i}>{v}</li>);
    let time = new Date().toISOString();

    if(this.handleValidation()){
      if (this.state.newRow === 0) {
        let data = {
          id,
          name,
          age,
          genderCheck,
          jobChoose,
          time
        };
        person.push(data);
      } else {
        //update
        let index = this.state.index;
        person[index].id = id;
        person[index].name = name;
        person[index].age = age;
        person[index].genderCheck = genderCheck;
        person[index].jobChoose = jobChoose;
        person[index].time = time;
      }
      this.setState({
        person: person,
        id: UUID.v4(),
        newRow: 0,
      });
      
    }else{
      alert("Form has errors.")
    }
    
  };

  //Pagination
  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({activePage: pageNumber});
  }

  //Function Delete
  delete = (i) =>{
    // alert('delete')
    let person = [...this.state.person]
    person.splice(i,1)
    this.setState({
      person
    });
  };

  // view personal imformation into Model Bootstrap.
  onView = (i) =>{
    let viewid = this.state.person[i].id;
    let viewname = this.state.person[i].name;
    let viewage = this.state.person[i].age;
    let viewgender = this.state.person[i].genderCheck;
    let viewjob = this.state.person[i].jobChoose;
    this.setState({
      viewID:viewid,
      viewName:viewname,
      viewAge:viewage,
      viewGender: viewgender,
      viewJob:viewjob
    })
    this.setState({
      show:true // used to show and hide Model bootstrap.
    })
  }
  // methods uset to show and hide Model bootstrap. 
  Hide=()=>{
    this.setState({
      show: false
    })
  }
  
   /************************** VIEW ON CARD ******************************/
   onViewCard = (i) => {
    let viewname = this.state.person[i].name;
    let viewgender = this.state.person[i].genderCheck;
    let viewjob = this.state.person[i].jobChoose;
    this.setState({
      viewName:viewname,
      viewGender: viewgender,
      viewJob:viewjob
    })
    this.setState({
      show:true // used to show and hide Model bootstrap.
    })
  }

  // ****************************** Show Card ***********************************/
  handleClick = (showList) => {
    this.setState({isClicked : true})
  }

  handleClickF = (showCard) => {
    this.setState({isClicked : false})
  }

  //update
  onUpdated = (id) =>{
    let i = id;
    let newId = this.state.person[id].id
    let newName = this.state.person[id].name
    let newAge = this.state.person[id].age
    let newGender = this.state.person[id].genderCheck
    let newJob = this.state.person[id].jobChoose
    alert("Person ID : "+newId + " wating to update!")

    this.setState({
      id:newId,
      name:newName,
      newAge:this.age,
      genderCheck:newGender,
      jobChoose: newJob,
      IdUpdate: i,
      btnUpdate:true
    })
  }

  onClickUpdate=(e)=>{
  e.preventDefault();
  const { gender } = this.form;
  const { job } = this.form;
  const checkboxArray = Array.prototype.slice.call(job);
  const checkedCheckboxes = checkboxArray.filter((input) => input.checked);
  const checkedCheckboxesValues = checkedCheckboxes.map(
    (input) => input.value
  );
  let ID = this.state.IdUpdate
  let newID = this.state.id
  let newName= this.name.value
  let newAge = this.age.value
  let newGender  = gender.value;
  let newJob = checkedCheckboxesValues.map((v, i) => <li key={i}>{v}</li>);
  console.log(newID,newName,newAge,newGender,newJob)
  let person = [...this.state.person]
  person[ID].id = newID 
  person[ID].name = newName
  person[ID].age = newAge
  person[ID].genderCheck = newGender
  person[ID].jobChoose = newJob
  this.setState({
    person,
    btnUpdate:false
  })
  alert("Person ID : "+ newID + "  get Updated!!!")
  }


  
  render() {
    let lastIndex = this.state.activePage * 3;
    let firstIndex = lastIndex - 3;
    let current = this.state.person.slice(firstIndex,lastIndex); //P
   
    let dataCard;
    let dataTable;
    const {isClicked} = this.state;
    if(isClicked){
    console.log("true"); 
    
    dataTable= current.map((data, i) => (
        <tr key={i}>
          <td>{data.id}</td>
          <td>{data.name}</td>
          <td>{data.age}</td>
          <td>{data.genderCheck}</td>
          <td>{data.jobChoose}</td>
          <td className="fontBattambang">
                <Moment fromNow locale='km' >{data.time}</Moment>
            </td>
            <td className="fontBattambang">
                 <Moment fromNow locale='km' >{data.time}</Moment>
            </td>
            <td>
                <Button variant="primary" onClick={()=>{this.onView(i)}}>View</Button>&nbsp;
                <Button variant="warning"  onClick={()=>{this.onUpdated(i)}}>Update</Button>&nbsp;
                <Button variant="danger" onClick={this.delete}>Delete</Button>
            </td>
        </tr>
    ));   
    }
    
    else if(!isClicked){
      console.log("false")

      dataCard = this.state.person.map((data, i) => (
        <Card style={{ width: '16rem',margin: "10px"}} key={i}> 
        <Card.Header>
        <DropdownButton variant="outline-primary" title="Action">
          <Dropdown.Item onClick={()=>{this.onViewCard(i)}}>View</Dropdown.Item>
          <Dropdown.Item onClick={this.delete}>Delete</Dropdown.Item>
          <Dropdown.Item >Upadate</Dropdown.Item>
        </DropdownButton>
        </Card.Header>
        <ListGroup variant="top">
        <ListGroup> <ul>{data.name}</ul>
                    <ul>Job : {data.jobChoose}</ul></ListGroup>
        </ListGroup>
        <Card.Footer className="fontBattambang" >
            <Moment fromNow locale='km' >{data.time}</Moment>
        </Card.Footer>
        </Card>
    ));
    }


    return (
      <div className="container">
        <form onSubmit={this.handleSubmit} ref={(form) => (this.form = form)}>
          <div className="container mt-5">
            <div className="row">
              <div className="col-md-12 text-center bg-dark text-white">
                <h1>Personal Information</h1>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
              <Form.Group controlId="">
                  <Form.Label>Name:</Form.Label>{" "}
                  <Form.Control
                    type="text"
                    name="name"
                    placeholder="Input Name"
                    onChange={this.handleChange.bind(this, "name")} value={this.state.fields["name"]}
                    ref={(inputName) => (this.name = inputName)}
                    
                  />
                  <span style={{color: "red"}}>{this.state.errors["name"]}</span>
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Label>Age:</Form.Label>
                  <Form.Control
                    type="text"
                    name="password"
                    placeholder="Input Age"
                    onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]}
                    ref={(inputAge) => (this.age = inputAge)}
                  />
                  <span style={{color: "red"}}>{this.state.errors["password"]}</span>
                </Form.Group>
                
              </div>
              <div className="col-md-4">
                <fieldset>
                  <Form.Group>
                    <Form.Label as="legend" column sm={2}>
                      Gender
                    </Form.Label>
                    <Col>
                      <Form.Check
                        type="radio"
                        label="Female"
                        name="gender"
                        inline
                        value="Female"
                      />
                      <Form.Check
                        type="radio"
                        label="Male"
                        name="gender"
                        inline
                        value="Male"
                      />
                    </Col>
                  </Form.Group>
                </fieldset>

                <fieldset>
                  <Form.Group>
                    <Form.Label as="legend" column sm={2}>
                      Job
                    </Form.Label>
                    <Col>
                      <Form.Check
                        type="checkbox"
                        label="Student"
                        name="job"
                        value="Student"
                        inline
                      />
                      <Form.Check
                        type="checkbox"
                        label="Teacher"
                        name="job"
                        value="Teacher"
                        inline
                      />
                      <Form.Check
                        type="checkbox"
                        label="Developer"
                        name="job"
                        value="Developer"
                        inline
                      />
                    </Col>
                  </Form.Group>
                </fieldset>
              </div>
            </div>
            <div className="row">
            {this.state.btnUpdate===false?<Button variant="dark" type="submit" value="Submit">Submit</Button>:<Button variant="info" onClick={this.onClickUpdate}>Update</Button>}
            </div>
            <div className="row text-center">
              <div className="col-md-12">
                Display Data: &nbsp;
                <Button variant="dark" onClick={() => this.handleClick("showList")}>List</Button>&nbsp;
                <Button variant="dark" onClick={() => this.handleClickF("showCard")}>Card</Button> 
              </div>
            </div>
          </div>

          <span className="row pt-5">
          {isClicked ? <span className="col-md-12"> {/*Condition Check True*/}
            <Table className="bg-light">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
                <th>Gender</th>
                <th>Job</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
              </tr>
            </thead> 
              <tbody>{dataTable}</tbody>
            </Table>
             <span className="row pt-3" style={{ display: "flex", justifyContent: "center", alignItems: "center" }} >
                <Pagination
                  prevPageText='Previous'
                  nextPageText='Next'
                  firstPageText='First'
                  lastPageText='Last'
                  activePage={this.state.activePage}
                  itemClass="page-item"
                  linkClass="page-link"
                  itemsCountPerPage={3}
                  totalItemsCount={this.state.person.length}
                  onChange={this.handlePageChange.bind(this)}
                />
             </span>
            {/* If False Show Card*/}
            </span> : 
              <span id="HorizontalCard">
                 {dataCard}
              </span>}
        </span>
        <div>
          <ViewIfo show={this.state.show} onHide={this.Hide}
            ViewID={this.state.viewID} ViewName={this.state.viewName}
            ViewAge={this.state.viewAge} ViewGender={this.state.viewGender}
            ViewJob={this.state.viewJob}
          />
        </div>
        <div>
          {/* View as Card */}
          <ViewCard show={this.state.show} onHide={this.Hide}
            ViewName = {this.state.viewName}
            ViewGender={this.state.viewGender}
            ViewJob={this.state.viewJob}
          />
        </div>
        </form>
      </div>
    );
  }
}
