import React from 'react'
import { Modal,Table,Button} from "react-bootstrap";
export default function ViewIfo(props) {

    
    return (
       

        <div>
            <Modal
                
                show={props.show}
                onHide ={props.onHide}
                dialogClassName="modal-90w"
                aria-labelledby="example-custom-modal-styling-title"
            >
                <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">
                    <h3>{props.ViewName}</h3>
                </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h6>Gender: {props.ViewGender}</h6> 
                    <h6>Jobs: {props.ViewJob}</h6>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

// render(<ViewIfo />);
